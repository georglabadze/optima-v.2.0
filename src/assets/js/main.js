function setSlickSlider (elem, param) {
	let slick = elem.slick({
		dots:            param.dots,
		draggable:       param.draggable,
		arrows:          param.arrows,
		prevArrow:       param.prevArrow,
		nextArrow:       param.nextArrow,
		infinite:        param.infinite,
		slidesToShow:    param.slidesToShow,
		responsive:      param.responsive,
		verticalSwiping: param.verticalSwiping,
		vertical:        param.vertical,
		autoplay:        param.autoplay,
		autoplaySpeed:   param.autoplaySpeed,
		variableWidth:   param.variableWidth
	});

	return slick;
}

function setSlick () {
	setSlickSlider($('#market-slider'), {
		arrows:         true,
		infinite:       false,
		centerMode:     true,
		dots:           false,
		variableWidth:  true,
		slidesToShow:   1,
		slidesToScroll: 1,
		prevArrow:      $('.market-slider-prev'),
		nextArrow:      $('.market-slider-next')

	})
}

function setScrollbars () {
	$('.market__list').overlayScrollbars({
		className:       "os-theme-dark main-scrollbar margin-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
	$('.market__dropdown-content').overlayScrollbars({
		className:       "os-theme-dark main-scrollbar margin-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});

	$('.trade-action-container').overlayScrollbars({
		className:       "os-theme-dark main-scrollbar margin-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});

	$('.trade-table-container').overlayScrollbars({
		className:       "os-theme-dark main-scrollbar",
		paddingAbsolute: true,
		scrollbars:      {
			clickScrolling: true
		}
	});
}

function marketListAccordeon () {
	$('.market__dropdown-content').hide();

	$('.market__dropdown-label').click(function () {
		$(this).parent().toggleClass('active').siblings().removeClass('active');
		$('.market__dropdown-content').stop().slideUp();

		if (!$(this).next().is(":visible")) {
			$(this).next().stop().slideDown();
		}
	});
}

function mobileMenu () {
	$('.hamburger').on('click', function () {
		$('.menu').stop().addClass('open');
		$('.o-nav').addClass('active');
		$('.nav-close-overlay').addClass('active');
	})
	$('.nav-close-overlay').click(function () {
		$(this).removeClass('active');
		$('.menu').removeClass('open')
		$('.o-nav').removeClass('active');
	})
	let win = $(window);
	if (win.width() <= 991) {
		$('.sidebar').addClass('fullscreen');
	} else {
		$('.sidebar').removeClass('fullscreen');
	}

	$(window).on('resize', function () {
		if (win.width() >= 991) {
			$('.sidebar').removeClass('fullscreen hidden');
			$('.o-table-section').show();
		} else {
			$('.sidebar').addClass('fullscreen');
			$('widget').show();
		}
	})
}

function valueCounter () {
	function customQuantity () {
		$('<span class="o-form__btn left mns"><i class="fal fa-minus"></i></span>').insertBefore(".counter");
		$('<span class="o-form__btn right pls"><i class="fal fa-plus"></i></span>').insertAfter(".counter");

		$(".o-form__wrapper").each(function () {
			var spinner    = $(this),
				input      = spinner.find('input[type="number"]'),
				btnPls     = spinner.find(".pls"),
				btnMns     = spinner.find(".mns"),
				min        = input.attr("min"),
				max        = input.attr("max"),
				valOfAmout = input.val(),
				newVal     = 0;

			btnPls.on("click", function (e) {
				e.preventDefault();
				var oldValue = parseFloat(input.val());

				if (oldValue >= max) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue + 1;
				}
				spinner.find("input").val(newVal);
				spinner.find("input").trigger("change");
			});
			btnMns.on("click", function (e) {
				e.preventDefault();
				var oldValue = parseFloat(input.val());
				if (oldValue <= min) {
					var newVal = oldValue;
				} else {
					var newVal = oldValue - 1;
				}
				spinner.find("input").val(newVal);
				spinner.find("input").trigger("change");
			});
		});
	}

	customQuantity();
}

function marketCardActive () {
	$('.back-btn').click(function () {
		$('.fullscreen').removeClass('hidden');
		$('.widget').show();
		$('.o-table-section').hide();
		$('.trade-action').show();
		$('.trade-info').removeClass('table');
	})
	$('.market__dropdown-card').click(function () {
		$('.fullscreen').addClass('hidden');
		$('.market__dropdown-card').removeClass('active');
		$(this).addClass('active');
	})
}

function favoriteStar () {
	$('.market__dropdown-card__favorite').click(function (e) {
		e.stopPropagation();
		$(this).toggleClass('active');
	})
}

function mobileTables () {
	$('.dark-btn').click(function (e) {
		e.preventDefault();
		let attr = $(this).attr('data-target');

		$('.sidebar').addClass('hidden');
		$('.trade-info').addClass('table');
		$('.trade-action').hide();
		$('.o-table-section').show();
		$('.nav-link').removeClass('active show');
		$('.tab-pane').removeClass('active show');
		$('#' + attr).addClass('active show');
		$('#' + attr + '-tab').addClass('active show');
	})
}

function userDropdowns () {
	$('.o-nav__user-dropdown').click(function () {
		$(this).closest('.o-nav__user').toggleClass('open');
	})
	$('.o-nav__lang-btn').click(function () {
		$(this).closest('.o-nav__lang').toggleClass('open');
	})
}

function minimizeTable () {
	$('.minimizer').click(function () {
		$(this).find('i').toggleClass('fa-eye fa-eye-slash');
		$(this).parent().toggleClass('hidden');
		if ($(this).parent().hasClass('hidden')) {
			$(this).find('span').text('show');
		} else {
			$(this).find('span').text('hide');
		}
	})
}

function toggleBtns () {
	function toggleBtn (target) {
		target.click(function (event) {
			event.preventDefault();
			target.removeClass('active');
			$(this).addClass('active');
		})
	}
	toggleBtn($('.o-form__wrapper.b-wrap .o-form__btn'))
	toggleBtn($('.modal-main .trade-form__wrapper .btn-main'))
}

function switcher () {
	$('.ex-material-switch').click(function () {
		if ($(this).children().is(':checked')){
			$(this).closest('.add-pending').find('.add-pending__container').fadeIn('fast');
			$('.buy-sell').addClass('disabled');
		} else{
			$(this).closest('.add-pending').find('.add-pending__container').fadeOut('fast');
			$('.buy-sell').removeClass('disabled');
		}
	})
}

$(function () {
	mobileMenu();
	setSlick();
	setScrollbars();
	valueCounter();
	favoriteStar(); //market list star
	marketCardActive(); // market list active
	userDropdowns();
	mobileTables();
	minimizeTable();
	marketListAccordeon();
	toggleBtns();
	switcher();
})
